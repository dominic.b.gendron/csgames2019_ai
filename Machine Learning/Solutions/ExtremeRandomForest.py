from sklearn.model_selection import cross_val_score

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import DecisionTreeClassifier

file = open("weatherData.txt", "r") 

X = []
Y = []

for x in file:
    line = x.rstrip().split(",")
    y = line.pop()
    X.append(line)
    Y.append(y)

#Entraine arbre 1
rndForest = RandomForestClassifier()
rndForest = rndForest.fit(X, Y)
rndForestPrediction= rndForest.predict([[5,6,15]])
print(rndForestPrediction)

#Entraine arbre 2
extraTree = ExtraTreesClassifier()
extraTree = extraTree.fit(X, Y)
extraTreePrediction = extraTree.predict([[5,6,15]])
print(extraTreePrediction)

#Entraine arbre 3
decisionTree = DecisionTreeClassifier()
decisionTree = decisionTree.fit(X, Y)
decisionTreePrediction = decisionTree.predict([[5,6,15]])
print(decisionTreePrediction)

scores = cross_val_score(rndForest, X, Y)
scores = cross_val_score(extraTree, X, Y)
scores = cross_val_score(decisionTree, X, Y)

#Moyenne des scores de certitudes sur leurs résultats
print(scores.mean())