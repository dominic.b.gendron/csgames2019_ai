from sklearn.ensemble import RandomForestClassifier

file = open("weatherData.txt", "r") 

X = []
Y = []

for x in file:
    line = x.rstrip().split(",")
    y = line.pop()
    X.append(line)
    Y.append(y)

clf = RandomForestClassifier(n_estimators=10)
clf = clf.fit(X, Y)

predicted = clf.predict([[5,6,15]])
print(predicted)