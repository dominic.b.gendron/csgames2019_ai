from sklearn.neural_network import MLPClassifier

# Les neural networks (ou multi layer perceptron) marchent seulement avec des floats
# On doit donc convertir les tags pluie et soleil en floats

# 1 = soleil
# 2 = pluie

def getStringFromInt(y):
    return "soleil" if y > 0.5 else "pluie"

def getFloatFromString(y):
    return 1.0 if y == "soleil" else 0.0


file = open("weatherData.txt", "r") 

X = []
Y = []

for x in file:
    line = x.rstrip().split(",")
    y = line.pop()
    yy = getFloatFromString(y)
    
    #Pire manière de convertir un array de string en float
    XX = []
    for xx in line:
        # /100 car les inputs doivent être normalisé entre [-1,1] ou [0,1]
        XX.append(float(xx) / 100)

    print(XX)
    print(yy)
    X.append(XX)
    Y.append(yy)

# hidden_layer_sizes c'est la grosseur du réseau de neurones [col, row]
# Un réseau trop gros est très long à calculé mais très accurate
# Un réseau peut petit se calcule très vite, mais peu accurate

# max_iter c'est le nombre de loop que l'algo fait. À chaque loop il va un peu mieux s'amélioré
# Pas assez de loop = aucune accuracy!
mlp = MLPClassifier(hidden_layer_sizes=(15, 5), max_iter=2000)
mlp = mlp.fit(X, Y)

predicted = mlp.predict([[5,6,15]])

# Convert le resultat
print(getStringFromInt(predicted))
