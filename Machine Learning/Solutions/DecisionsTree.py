from sklearn import tree

file = open("weatherData.txt", "r") 

X = []
Y = []

for x in file:
    line = x.rstrip().split(",")
    y = line.pop()
    X.append(line)
    Y.append(y)

clf = tree.DecisionTreeClassifier()
clf = clf.fit(X, Y)

predicted = clf.predict([[5,6,15]])
print(predicted)