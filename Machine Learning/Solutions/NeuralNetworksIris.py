from sklearn.neural_network import MLPClassifier
from sklearn.datasets import load_iris

def getStringFromPrediction(y):
    if y < -0.5:
        return "Iris Setosa"
    elif y < 0.5:
        return "Iris Versicolour"
    elif y >= 0.5:
        return "Iris Virginica"
    return 0

iris = load_iris()

X = iris.data
Y = iris.target

print(X)
print(Y)

mlp = MLPClassifier(hidden_layer_sizes=(15, 5), max_iter=2000)
mlp = mlp.fit(X, Y)

predicted = mlp.predict([[5.8,2.8,5.1,2.4]])
print(predicted)
print(getStringFromPrediction(predicted))
