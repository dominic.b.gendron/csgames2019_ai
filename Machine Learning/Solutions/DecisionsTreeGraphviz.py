from sklearn import tree
import graphviz 


file = open("weatherData.txt", "r") 

X = []
Y = []

for x in file:
    line = x.rstrip().split(",")
    y = line.pop()
    X.append(line)
    Y.append(y)

clf = tree.DecisionTreeClassifier()
clf = clf.fit(X, Y)

predicted = clf.predict([[5,6,15]])
print(predicted)

#Graphviz
dot_data = tree.export_graphviz(clf, out_file=None) 
graph = graphviz.Source(dot_data) 
graph.render("weatherDataTree") 

feature_name = ["Temperature", "Vent", "Humidité"]
class_name = "Temps"

dot_data = tree.export_graphviz(clf, out_file=None,feature_names=feature_name,class_names=class_name,filled=True, rounded=True,special_characters=True)  
graph = graphviz.Source(dot_data)  

