# Machine Learning

On souhaite prédire s'il fera soleil le jour des CS GAMES

On recueille donc des données sur le weather de la semaine dernière.
Les donnés seront pour prédire s'il fera soleil ou pleuveras.
Dans le fichier weatherData.txt se trouve tous les données.
Chaque ligne correspond à une échantillon de donnée.
Chaque échantillon contients plusieurs attributs comme suit

[Temperature, vitesse du vent, humidité, pluie/soleil]

Le jour des CSGAMES, les données sont

[Temperature = 5, vitesse du vent = 6, humidité = 15, ???] 
( [5,6,15] )

Il faut donc trouvé s'il fera soleil ou pas!


## Decision Tree

https://scikit-learn.org/stable/modules/tree.html

1) Determiner s'il fera soleil ou pas
2) Implémenter graphiz comme dans l'exemple (optionel)

### Clairifcation

Dans cette librarie, les X sont les attributs
Le Y est le résultat (soleil/pluie)

Il faut donc faire un [[]] pour les X pour chaque jour et chaque data
ex : 
[24, 0, 95]
[35, 15, 5]
[5, 35, 45]]
et un [] pour les Y
[pluie, soleil, pluie]

Il se peut que des "\n" apparaissent dans les datas, utilisé '.rstrip()' 

## Random Forests

https://scikit-learn.org/stable/modules/ensemble.html

On apprend qu'un Random Forest a une meilleur capacité de prédiction. 
En effet, le random forests distribut aléatoirement les échantillons entre plusieurs "Decision Tree" 
et demande à chaque "Decision Tree" de prédire le résultat, le vote majoritaire gagne!

1) Faite une prédiction avec le RandomForstClasisfier avec les mêmes data qu'avant
2) Lisez sur le "Extremely Randomized Trees" dans la page web, et faite une prédiction

## Neural Networks


Vous lisez le journal et vous vous rendez compte qu'on parle de réseau de neurones partout!
C'EST LE TEMPS D'EN FAIRE UN FROM SCRATCH

1) Lire le livre https://www.deeplearningbook.org/ au complet (ou pas, c'est fucking hard)
2) Regarder la série pour avoir une meilleur intuition https://www.youtube.com/watch?v=aircAruvnKk
3) Essayer de prédire le temps avec les mêmes datas en utilisant le MLPClassifier


Vous allez surement avoir plus de misère, les réseaux de neurones prennent seulement des floats et non des labels comme "pluie/soleil"
Il faut donc s'assurez que tout les inputs soient en float!
Je propose que "soleil" = 1 et "pluie" = 0
Le résultats sera en float aussi, il faut donc le convertir en string (if result > 0.5f) = soleil else pluie

https://scikit-learn.org/stable/modules/neural_networks_supervised.html

## Neural Networks Iris

 1) Importez les données de "iris" et essayez de prédire le type de "iris"

`from sklearn.datasets import load_iris`

`iris = load_iris()`

`X = iris.data`

`Y = iris.target` 


Voici les infos des données
___

1. sepal length in cm 
2. sepal width in cm 
3. petal length in cm 
4. petal width in cm 
5. class: 
-- Iris Setosa = 0
-- Iris Versicolour = 1
-- Iris Virginica = 2
___

Prédire 
X = 5.8,2.8,5.1,2.4

Le résultat doit être 
Y = Iris-virginica = 2





