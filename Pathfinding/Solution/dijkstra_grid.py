import dijkstra

def solve(newmap, xStart, yStart, xEnd, yEnd, collisions):
	
	#Fonction pour normaliser les noms de nodes par rapport a leur position
	def getNodePositionName(x,y):
		return "{0},{1}".format(x,y)
		
		#On try tous les positions adjacent
	def tryAddNodes (x,y):
		pos = getNodePositionName(x,y)
		tryAddNode(pos, x+1, y)
		tryAddNode(pos, x-1, y)
		tryAddNode(pos, x, y+1)
		tryAddNode(pos, x, y-1)
		
	#On v�rifie si la position est valide, si oui on la rajoute a la liste 
	def tryAddNode (pos, x,y):
		
		#Out of bound
		if x < 0 or x >= sizeX:
			return
		if y < 0 or y >= sizeY:
			return
		
		# Un mur
		for c in collisions:
			if map[x][y] == c :
				return
		
		node = pos, getNodePositionName(x,y), 1
		nodes.append(node)
	
	map = newmap
	x = 0
	y = 0
	sizeX = len(map)
	sizeY = len(map[0])
	nodes = []
	
	#On parcours la map 
	for string in map:
		for char in string:
			tryAddNodes(x,y)
			y+=1
		x+=1
		y=0
		
	graph = dijkstra.Graph(nodes)

	start = getNodePositionName(xStart,yStart)
	end = getNodePositionName(xEnd,yEnd)

	return graph.dijkstra(start,end)